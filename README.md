# Javascript implementation of the OpenSCAD language


## Prerequisites

* node.js

## Get started

    npm install
    npm test

**Run tests manually:** (mocha is configured in _test/mocha.opts_ and _.babelrc_)

    mocha -g <regex>

**.scad -> .scad**

    ./scad2scad.js testdata/test-square.scad [-o out.scad]


## jison notes

### Generate a parser vs. creating a parser at run-time:

**Generate a parser:**

```
npm install -g jison
jison lib/openscad.jison
```

**Create parser at run-time:**

This is what we do - see parser.js

### jison file format vs. JSON grammar

We can use .jison (and optionally a separate .jsonlex) files to describe a grammar. Alternatively, it's possible to describe grammars using a JSON format which contains the lexer and BNF grammar.

We use a .jison file containing both lexer and grammar.

## How to create railroad diagrams

1. ```node createbnf.js lib/openscad.jison -o out.ebnf```
2. Go to http://bottlecaps.de/rr/ui
3. Paste EBNF file into the "Edit Grammar" tab
4. View Diagram -> Download as SVG

-> This gives us an XHTML file with embedded SVGs


## Debugging

```
$ npm install -g node-inspector babel-node-debug
```

```
$ node-debug _mocha -g resize
```

