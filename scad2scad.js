#!/usr/bin/env ./node_modules/.bin/babel-node

import path from 'path';
import * as tools from './cmdline-tools';
import {parser} from './dynamicparser';

var opts = require('nomnom')
  .script('scad2scad')
  .option('file', {
    flag: true,
    position: 0,
    help: '.scad file'
  })
  .option('output', {
    abbr: 'o',
    help: '.scad output file'
  })
  .parse();

async function main() {
  const source = await tools.readAll(opts.file);
  const dirname = opts.file ? path.dirname(path.normalize(opts.file)) : '';
  const ast = parser.parse(source);
  tools.writeAll(opts.output, ast);
}

if (require.main === module) {
  // Note: the async main() function will swallow exceptions and make them available only as 
  // a promise failure
  main().catch(err => {
    console.error(`ERROR: ${err.message}`);
    process.exit(1);
  });
}
