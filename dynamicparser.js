import fs from 'fs';
import fsp from 'fs-promise';
import jison from 'jison';
import * as AST from './lib/ast';

function createParser(bnf) {
  const dynamicparser = new jison.Parser(bnf);
  dynamicparser.yy = {AST: AST};
  return dynamicparser;
}

function resourceLoader(path) {
  return fsp.readFile(path, 'utf8');
}

const parser = createParser(fs.readFileSync(__dirname+'/lib/openscad.jison', 'utf8'));

global.OpenSCAD = {
  parser: parser,
  resourceLoader: resourceLoader
};

export { parser, resourceLoader };
