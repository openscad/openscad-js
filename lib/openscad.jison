/* lexical grammar */
%lex

int      [0-9]|[1-9][0-9]+
exp      [eE][-+]?[0-9]+
frac     "."[0-9]+
str      (?:'\\'[\\"bfnrt/]|'\\u'[a-fA-F0-9]{4}|[^\\\0-\x09\x0a-\x1f"])*
filename ([^\\\0-\x09\x0a-\x1f"])+

%s include use

%%

"include"             this.begin('include')
"use"                 this.begin('use')
<use>"<"{filename}">"     { this.popState(); yytext = yytext.substr(1,yyleng-2); return 'USE'; }
<include>"<"{filename}">" { this.popState(); yytext = yytext.substr(1,yyleng-2); return 'INCLUDE'; }
\s+                   /* skip whitespace */
"//".*                /* ignore comment */
"/*"[\w\W]*?"*/"      /* ignore comment */
"let"                 return 'LET'
"true"                return 'TRUE'
"false"               return 'FALSE'
"for"                 return 'FOR'
"undef"               return 'UNDEF'
"function"            return 'FUNCTION'
"module"              return 'MODULE'
"if"                  return 'IF'
"else"                return 'ELSE'
{int}{frac}?{exp}?\b  return 'NUMBER'
{int}?{frac}{exp}?\b  return 'NUMBER'
\"{str}\"             yytext = yytext.substr(1,yyleng-2); return 'STRING'
";"                   return ';'
":"                   return ':'
"+"                   return '+'
"-"                   return '-'
"/"                   return '/'
"*"                   return '*'
"%"                   return '%'
">="                  return 'GE'
">"                   return '>'
"=="                  return 'EQ'
"!="                  return 'NE'
"<="                  return 'LE'
"<"                   return '<'
"&&"                  return 'AND'
"||"                  return 'OR'
"("                   return '('
")"                   return ')'
"["                   return '['
"]"                   return ']'
"{"                   return '{'
"}"                   return '}'
","                   return ','
"="                   return '='
"?"                   return '?'
"!"                   return '!'
"#"                   return '#'
"$"?\w+               return 'ID'

/lex

%start package 

%right let_clause
%right '?' ':'
%right IF ELSE

%left OR
%left AND
%left '<' LE GE '>'
%left EQ NE
%left '!' '+' '-'
%left '*' '/' '%'
%left '[' ']'
%left '.'

%% /* language grammar */

package: statements { $$ = new yy.AST.Package($1, @$); return $$; };

statements:
    statement { $$ = [$1] } |
    statements statement { $$ = $1; if ($2 !== null) $$.push($2) } ;

statement:
    include |
    use |
    assignment ';' |
    named_function_definition |
    named_module_definition |
    ';' { $$ = null } |
    '{' child_statements '}' { $$ = $2 } |
    conditional_statement |
    module_instantiation ;

file:
    '<' filename '>' { $$ = $2 } ;

include:
    INCLUDE { $$ = new yy.AST.Include($1, @$); } ;

use:
    USE { $$ = new yy.AST.Use($1, @$); } ;

comma_opt:
    /* empty */ |
    ',' ;

expressions_opt:
    comma_opt { $$ = [] } |
    expressions comma_opt ;

expressions:
    expr { $$ = [$1] } |
    expressions ',' expr { $$ = $1; $$.push($3) } ;

expr:
    TRUE { $$ = new yy.AST.Literal(true, @$) } |
    FALSE { $$ = new yy.AST.Literal(false, @$) } |
    UNDEF { $$ = new yy.AST.Literal(undefined, @$) } |
    NUMBER { $$ = new yy.AST.Literal(parseFloat($1), @$) } |
    STRING { $$ = new yy.AST.Literal($1, @$) } |
    lookup |
    range_expression |
    list_expression |
    expr '+' expr { $$ = new yy.AST.BinaryOp($2, $1, $3, @$) } |
    expr '-' expr { $$ = new yy.AST.BinaryOp($2, $1, $3, @$) } |
    expr '*' expr { $$ = new yy.AST.BinaryOp($2, $1, $3, @$) } |
    expr '/' expr { $$ = new yy.AST.BinaryOp($2, $1, $3, @$) } |
    expr '%' expr { $$ = new yy.AST.BinaryOp($2, $1, $3, @$) } |
    expr GE expr  { $$ = new yy.AST.BinaryOp($2, $1, $3, @$) } |
    expr '>' expr { $$ = new yy.AST.BinaryOp($2, $1, $3, @$) } |
    expr EQ expr  { $$ = new yy.AST.BinaryOp($2, $1, $3, @$) } |
    expr NE expr  { $$ = new yy.AST.BinaryOp($2, $1, $3, @$) } |
    expr LE expr  { $$ = new yy.AST.BinaryOp($2, $1, $3, @$) } |
    expr '<' expr { $$ = new yy.AST.BinaryOp($2, $1, $3, @$) } |
    expr AND expr { $$ = new yy.AST.BinaryOp($2, $1, $3, @$) } |
    expr OR expr  { $$ = new yy.AST.BinaryOp($2, $1, $3, @$) } |
    '!' expr { $$ = new yy.AST.UnaryOp($1, $2, @$) } |
    '+' expr { $$ = new yy.AST.UnaryOp($1, $2, @$) } |
    '-' expr { $$ = new yy.AST.UnaryOp($1, $2, @$) } |
    expr '?' expr ':' expr { $$ = new yy.AST.ConditionalOp($1, $3, $5, @$) } |
    expr '[' expr ']' { $$ = new yy.AST.ArrayLookup($1, $3, @$) } |
    '(' expr ')' { $$ = $2 } |
    '[' list_comprehension_elements ']' { $$ = new yy.AST.ListComprehension($2, @$) } |
    let_clause expr { $$ = new yy.AST.Scope($1, $2, @$) } |
    function_call
    ;

expr_opt:
    /* empty */ |
    expr ;

lookup:
    ID { $$ = new yy.AST.Lookup($1, @$) } ;

range_expression:
    '[' expr ':' expr ']' { $$ = new yy.AST.Range($2, new yy.AST.Literal(1), $4, @$) } |
    '[' expr ':' expr ':' expr ']' { $$ = new yy.AST.Range($2, $4, $6, @$) };

list_expression:
    '[' expressions_opt ']' { $$ = new yy.AST.List($2, @$) } ;

list_comprehension_elements:
    let_clause list_comprehension_elements { $$ = new yy.AST.ListComprehensionLet($1, $2, @$) } |
    for_clause list_comprehension_elements_or_expr { $$ = new yy.AST.ListComprehensionFor($1, $2, @$) } |
    if_clause list_comprehension_elements_or_expr { $$ = new yy.AST.ListComprehensionIf($1, $2, @$) } ;

list_comprehension_elements_or_expr:
    list_comprehension_elements |
    expr ;

let_clause:
    LET '(' assignments_opt ')' { $$ = $3 };

for_clause:
    FOR '(' assignments ')' { $$ = $3 };

if_clause:
    IF '(' expr ')' { $$ = $3 };

function_definitions_opt:
    /* empty */ |
    function_definitions ;

function_definitions:
    named_function_definition { $$ = [$1] } |
    function_definitions named_function_definition { $$ = $1; $$.push($2) } ;

named_function_definition:
    FUNCTION ID '(' parameters_opt ')' '=' expr ';' { $$ = new yy.AST.NamedFunction($2, new yy.AST.UserFunction($4, $7, @$), @$) } ;

module_definitions_opt:
    /* empty */ |
    module_definitions ;

module_definitions:
    named_module_definition { $$ = [$1] } |
    module_definitions named_module_definition { $$ = $1; $$.push($2) } ;

named_module_definition:
    MODULE ID '(' parameters_opt ')' module_body { $$ = new yy.AST.NamedModule($2, new yy.AST.UserModule($4, $6, @$), @$) } ;

module_body:
  ';' { $$ = [] } |
  module_instantiation { $$ = [$1] } |
  '{' module_body_statements_opt '}' { $$ = $2 } |
  conditional_statement { $$ = [$1] } ;

module_body_statements_opt:
    /* empty */ { $$ = [] } |
    statements ;

function_call:
    ID '(' arguments_opt ')' { $$ = new yy.AST.FunctionCall($1, $3, @$) } ;

modifier:
    '#' |
    '%' |
    '*' |
    '!' ;

module_instantiation:
    modifier module_instantiation { $$ = $2; $$.setModifier($1); } |
    ID '(' arguments_opt ')' instantiation_children { $$ = new yy.AST.ModuleInstantiation($1, $3, $5, @$) } |
    FOR '(' arguments_opt ')' instantiation_children { $$ = new yy.AST.ForLoop($3, $5, @$) } ;

// Always returns an array
instantiation_children:
    other_child_statement |
    conditional_statement { $$ = [$1] };

conditional_statement:
    modifier conditional_statement { $$ = $2; $$.setModifier($1); } |
    IF '(' expr ')' instantiation_children { $$ = new yy.AST.IfElse($3, $5, null, @$) } |
    IF '(' expr ')' instantiation_children ELSE instantiation_children { $$ = new yy.AST.IfElse($3, $5, $7, @$) } ;

other_child_statement:
    ';' { $$ = [] } |
    '{' child_statements '}' { $$ = $2 } |
    module_instantiation { $$ = [$1] };

child_statements:
    /* empty */ { $$ = [] } |
    child_statements child_statement { $$ = $1; $$.push($2) } ;

child_statement:
    '{' child_statements '}' { $$ = $2 } |
    module_instantiation { $$ = $1 } |
    conditional_statement { $$ = $1 } |
    assignment ';' ;

parameters_opt:
    /* empty */ { $$ = [] } |
    parameters ;

parameters:
    parameter { $$ = [$1] } |
    parameters ',' parameter { $$ = $1; $$.push($3) } ;

parameter:
    lookup |
    assignment ;

arguments_opt:
    /* empty */ { $$ = [] } |
    arguments ;

arguments:
    argument { $$ = [$1] } |
    arguments ',' argument { $$ = $1; $$.push($3) } ;

argument:
    expr |
    assignment ;

assignments_opt:
    /* empty */ { $$ = [] } |
    assignments ;

assignments:
    assignment { $$ = [$1] } |
    assignments ',' assignment { $$ = $1; $$.push($3) };

assignment:
    ID '=' expr { $$ = new yy.AST.Assignment($1, $3, @$) } ;

