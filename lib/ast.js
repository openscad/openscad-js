import assert from 'assert';
import _ from 'lodash';
const debug = require('debug')('ast'); // eslint-disable-line
import {generate as shortid} from 'shortid';


export class ASTNode {
  constructor(srcref) {
    this.id = shortid();
    if (srcref) {
      const {first_line, first_column, last_line, last_column} = srcref;
      this.sourceidx = [[first_line, first_column], [last_line, last_column]];
    }
  }

  get type() {
   return this.constructor.name;
  }
}

export class Assignment extends ASTNode {

  constructor(lhs, rhs, srcref) {
    super(srcref);
    this.name = lhs;
    this.expr = rhs;
  }

  toString() {
    return `${this.name} = ${this.expr}`;
  }
}

export class Package extends ASTNode {

  constructor(statements, srcref) {
    super(srcref);
    this.statements = statements;
  }

  toString(indent = '', space = 2) {
    // Stand-alone assignments need semicolon
    return this.statements.map(
      statement => `${statement.toString(indent, space)}${(statement instanceof Assignment) ? ';' : ''}`
    ).join('\n');
  }
}

export class Program extends ASTNode {

  constructor(functions, expr, srcref) {
    super(srcref);
    this.functions = functions;
    this.expr = expr;
  }

  toString() {
    return (this.functions ? this.functions.toString() + ' ' : '') + this.expr.toString();
  }
}

export class Literal extends ASTNode {

  constructor(value, srcref) {
    super(srcref);
    this.value = value;
  }

  toString() {
    if (this.value === undefined) return 'undef';
    else if (typeof this.value === 'string') return '"' + this.value.toString() + '"';
    return this.value.toString();
  }
}

export class Range extends ASTNode {

  constructor(begin, step, end, srcref) {
    super(srcref);
    this.begin = begin;
    this.step = step;
    this.end = end;
  }

  toString() {
    return `[${this.begin} : ${this.step} : ${this.end}]`;
  }
}

export class Lookup extends ASTNode {

  constructor(name, srcref) {
    super(srcref);
    this.name = name;
  }

  toString() { return this.name; }
}

export class ArrayLookup extends ASTNode {

 constructor(nameexpr, idxexpr, srcref) {
   super(srcref);
   this.nameexpr = nameexpr;
   this.idxexpr = idxexpr;
 }

  toString() {
    return `${this.nameexpr}[${this.idxexpr}]`;
  }
}

export class BinaryOp extends ASTNode {

  constructor(op, left, right, srcref) {
    super(srcref);
    this.op = op;
    this.left = left;
    this.right = right;
  }

  precedence() {
    switch (this.op) {
    case '+':
    case '-':
      return 5;
    case '*':
    case '/':
      return 6;
    default:
      return 0;
    }
  }

  toString() {
    // If left is a binary operator and has lower precedence than this:
    //    add parenth around left
    const lp = this.left instanceof BinaryOp &&
      this.left.precedence() < this.precedence();

    // If right is a binary operator and has lower precedence than this:
    //    add parenes around right
    const rp = this.right instanceof BinaryOp &&
      this.right.precedence() < this.precedence();

    return (lp?'(':'') + this.left + (lp?')':'') + ' ' + this.op + ' '
      + (rp?'(':'') + this.right + (rp?')':'');
  }
}

export class UnaryOp extends ASTNode {

  constructor(op, expr, srcref) {
    super(srcref);
    this.op = op;
    this.expr = expr;
  }

  toString() {
    return this.op + this.expr;
  }
}

export class ConditionalOp extends ASTNode {

  constructor(condition, ifexpr, elseexpr, srcref) {
    super(srcref);
    this.condition = condition;
    this.ifexpr = ifexpr;
    this.elseexpr = elseexpr;
  }

  toString() {
    return `${this.condition} ? ${this.ifexpr} : ${this.elseexpr}`;
  }
}

export class Scope extends ASTNode {

  constructor(assignments, expr, srcref) {
    super(srcref);
    this.assignments = assignments;
    this.expr = expr;
  }

  toString() {
    return `let(${this.assignments.join(', ')}) ${this.expr}`;
  }
}

export class ListComprehensionLet extends ASTNode {

  constructor(assignments, expr, srcref) {
    super(srcref);
    this.assignments = assignments;
    this.expr = expr;
  }

  toString() {
    return `let(${this.assignments.join(', ')}) ${this.expr}`;
  }
}

export class ListComprehensionFor extends ASTNode {

  constructor(assignments, expr, srcref) {
    super(srcref);
    // Rewrite "for (i=.., j=..)" to "for(i=..) for (j=..)"
    if (Array.isArray(assignments)) {
      let prev = expr;
      for (let i=assignments.length-1; i>0; i--) {
        prev = new ListComprehensionFor(assignments[i], prev);
      }
      this.assignment = assignments[0];
      this.expr = prev;
    }
    else {
      this.assignment = assignments;
      this.expr = expr;
    }
  }

  toString() {
    return `for (${this.assignment}) ${this.expr}`;
  }
}

export class ListComprehensionIf extends ASTNode {

  constructor(condition, expr, srcref) {
    super(srcref);
    this.condition = condition;
    this.expr = expr;
  }

  toString() {
    return `if (${this.condition}) ${this.expr}`;
  }
}

export class ListComprehension extends ASTNode {

  constructor(expr, srcref) {
    super(srcref);
    this.expr = expr;
  }

  toString(indent, space) {
    return '[' + this.expr.toString() + ']';
  }
}

export class FunctionCall extends ASTNode {

  constructor(name, args, srcref) {
    super(srcref);
    this.name = name;
    this.args = args;
  }

  toString() {
    return this.name + '(' + _.map(this.args, arg => arg.toString()).join(', ') + ')';
  }
}

function childrenString(children, indent, space) {
  if (typeof space === 'number') space = _.repeat(' ', space);

  if (children.length === 0) return ';';
  if (children.length === 1) return ' ' + children[0].toString(indent, space);
  const chstr = children.map(ch =>
      indent + space + ch.toString(indent + space, space) + ((ch instanceof Assignment) ? ';' : '')
    ).join('\n');
  return ` {\n${chstr}\n${indent}}`;
}

export class ModuleInstantiation extends ASTNode {

  constructor(name, args, children, srcref) {
    super(srcref);
    assert(Array.isArray(children));

    this.name = name;
    this.args = args; // Array of Expression or Assignment
    this.children = children;
  }

  setModifier(m) {
    if (!this.modifier) this.modifier = '';
    this.modifier = m + this.modifier;
  }

  callString() {
    return `${this.modifier ? this.modifier : ''}${this.name}(${this.args})`;
  }

  toString(indent, space) {
    return this.callString() + childrenString(this.children, indent, space);
  }
}

export class ForLoop extends ModuleInstantiation {

  constructor(args, children, srcref) {
    // Rewrite "for (i=.., j=..)" to "for(i=..) for (j=..)"
    debug('ForLoop: ' + JSON.stringify(args));

    // The parser will wrap it in an array
    assert(Array.isArray(args));

    if (Array.isArray(args)) {
      let prev = children;
      for (let i=args.length-1; i>0; i--) {
        prev = [new ForLoop(args.slice(i, i+1), prev, srcref)];
      }
      super('for', args.slice(0, 1), prev, srcref);
    }
  }
}

export class IfElse extends ModuleInstantiation {

  constructor(expr, children, else_children, srcref) {
    super('if', expr !== undefined ? [expr] : [], children, srcref);
    if (else_children !== null) this.else_children = else_children;
  }

  toString(indent='', space=4) {
    let elsestr = '';
    if (this.else_children && this.else_children.length > 0) {
      elsestr = `${this.children.length > 1 ? '\n' : ' '}else${childrenString(this.else_children, indent, space)}`;
    }
    return `${super.toString(indent, space)}${elsestr}`;
  }
}

export class NamedFunction extends ASTNode {

  constructor(name, func, srcref) {
    super(srcref);
    this.name = name;
    this.func = func;
  }

  toString() {
    return `function ${this.name}(${this.func.params.join(', ')}) = ${this.func.expr};`;
  }
}

export class AbstractFunction extends ASTNode {
  constructor(srcref) {
    super(srcref);
  }
}

/*
  Function expression
  params = array of parameters (lookup or assignment)
  expr = single expression
*/
export class UserFunction extends AbstractFunction {

  constructor(params, expr) {
    super();
    this.params = params;
    this.expr = expr;
  }

  getPositionalParameter(pos) {
    return this.params[pos];
    // FIXME: Warning if requestiong a parameter out of bounds?
  }


/*
  Binds a function to a lexical scope.
*/
  bind(context) {
    this.lexicalscope = context;
  }

  toString() {
    return `function (${this.params.join(', ')}) ${this.expr}`;
  }
}

export class NamedModule extends ASTNode {

  constructor(name, module, srcref) {
    super(srcref);
    this.name = name;
    this.module = module;
  }

  headString() {
    return `module ${this.name}(${this.module.params})`;
  }

  toString(indent, space) {
    return `${this.headString()}${childrenString(this.module.body, indent, space)}`;
  }
}

export class Module extends ASTNode {

  // params is optional
  constructor(params, srcref) {
    super(srcref);
    this.params = params; // array of (Lookup || Assignment)
  }

  getPositionalParameter(pos) {
    // Modules like "echo" may provide parameters outside the declared range,
    // so we need to be robust.
    return (this.params && this.params.length > pos) ? this.params[pos] : undefined;
  }
}

/*
  Module definition
  params = array of parameters (lookup or assignment)
  body = list of statements (ModuleInstantiation, Assignment, FIXME: use/include)
*/
export class UserModule extends Module {

  constructor(params, body) {
    super(params);
    this.body = body;
  }

  /*
    Binds a module to a lexical scope.
  */
  bind(context) {
    this.lexicalscope = context;
  }

  toString(indent, space) {
    return `module (${this.params.join(', ')}) ${childrenString(this.body, indent, space)}`;
  }
}

export class List extends ASTNode {

  constructor(items, srcref) {
    super(srcref);
    this.items = items;
  }

  toString() {
    return `[${this.items}]`;
  }
}

export class Include extends ASTNode {

  constructor(path, srcref) {
    super(srcref);
    this.path = path;
  }

  toString() {
    return `include <${this.path}>`;
  }
}

export class Use extends ASTNode {

  constructor(path, srcref) {
    super(srcref);
    this.path = path;
  }

  toString() {
    return `use <${this.path}>`;
  }
}
