const expect = require('chai').expect;
import {parser} from './test-tools';

describe('AST', function() {
  function passthrough(statement, res) {
    if (res === undefined) res = statement;
    expect(parser.parse(statement).toString()).to.equal(res);
  }
  function exprPassthrough(expr, res) {
    if (res === undefined) res = expr;
    const src = 'a = '+expr+';'; // Turn expression into a valid statement
    res = 'a = '+res+';';
    return passthrough(src, res);
  }
  describe('literals', function() {
    it('integer', function() {
      exprPassthrough('1');
    });
    it('decimal', function() {
      exprPassthrough('1.2');
    });
    it('decimal without zero', function() {
      const e = '.2';
      exprPassthrough(e, '0.2');
    });
    it('negative decimal without zero', function() {
      const e = '-.2';
      exprPassthrough(e, '-0.2');
    });
    it('scientific', function() {
      const e = '1.2e-2';
      exprPassthrough(e, '0.012');
    });
    it('true', function() {
      exprPassthrough('true');
    });
    it('true', function() {
      exprPassthrough('false');
    });
    it('undef', function() {
      exprPassthrough('undef');
    });
    it('string', function() {
      exprPassthrough('"some string"');
    });
    it('range', function() {
      const e = '[0 : 5]';
      exprPassthrough(e, '[0 : 1 : 5]');
    });
    it('range with step', function() {
      exprPassthrough('[0 : 0.5 : 5]');
    });
    it('vector', function() {
      exprPassthrough('[0,1,2,3]');
    });
    it('vector with trailing comma', function() {
      const e = '[0,1,2,3,]';
      exprPassthrough(e, '[0,1,2,3]');
    });
    it('matrix', function() {
      exprPassthrough('[[0,1,2],[3,4,5],[6,7,8]]');
    });
  });
  describe('operators', function() {
    it('*', function() {
      exprPassthrough('1 * 2');
    });
    it('/', function() {
      exprPassthrough('1 / 2');
    });
    it('+', function() {
      exprPassthrough('1 + 2');
    });
    it('-', function() {
      exprPassthrough('1 - 2');
    });
    it('- without whitespace', function() {
      const e = 'a=3-2;';
      expect(parser.parse(e).toString()).to.equal('a = 3 - 2;');
    });
    it('%', function() {
      exprPassthrough('1 % 2');
    });
    it('>', function() {
      exprPassthrough('1 > 2');
    });
    it('>=', function() {
      exprPassthrough('1 >= 2');
    });
    it('==', function() {
      exprPassthrough('1 == 2');
    });
    it('!=', function() {
      exprPassthrough('1 != 2');
    });
    it('<=', function() {
      exprPassthrough('1 <= 2');
    });
    it('<', function() {
      exprPassthrough('1 < 2');
      exprPassthrough('1 < a || a > 2'); // Make sure include/use parsing doesn't get in the way
    });
    it('&&', function() {
      exprPassthrough('1 && 2');
    });
    it('||', function() {
      exprPassthrough('1 || 2');
    });
    it('parentheses', function() {
      exprPassthrough('(1 + 2) * 3');
      exprPassthrough('3 * (1 + 2)');
    });
    it('?:', function() {
      exprPassthrough('false ? 2 : 3');
    });
    it('!', function() {
      exprPassthrough('!a');
    });
    it('+', function() {
      exprPassthrough('+a');
    });
    it('-', function() {
      exprPassthrough('-a');
    });
  });
  describe('list comprehensions', function() {
    it('for list', function() {
      exprPassthrough('[for (i = [0,1,2]) i]');
    });
    it('for range', function() {
      exprPassthrough('[for (i = [0 : 1 : 2]) i]');
    });
    it('for range let', function() {
      exprPassthrough('[for (i = [0 : 1 : 2]) let(a = i) a]');
    });
    it('for range if', function() {
      exprPassthrough('[for (i = [0 : 1 : 2]) if (i != 1) i]');
    });
    it('let for', function() {
      exprPassthrough('[let(a = 1) for (i = [a : 1 : 4]) i]');
    });
    it('for multiple assignments', function() {
      const e = 'a = [for (x = 1, y = 2) x + y];';
      expect(parser.parse(e).toString()).to.equal('a = [for (x = 1) for (y = 2) x + y];');
    });
    it('index into generated list', function() {
      exprPassthrough('[for (i = [0,1,2]) i][1]');
    });
  });
  describe('definitions, lookup, scope', function() {
    it('value assignment', function() {
      const e = 'a = 4;';
      expect(parser.parse(e).toString()).to.equal(e);
    });
    it('lookup', function() {
      exprPassthrough('a');
    });
    it('array lookup', function() {
      exprPassthrough('a[b]');
      exprPassthrough('a[b][c]');
    });
    it('matrix lookup', function() {
      exprPassthrough('a[b][c]');
    });
    it('let', function() {
      exprPassthrough('let(b = 1) b');
    });
    it('empty let', function() {
      exprPassthrough('let() 3');
    });
    it('multiple let', function() {
      exprPassthrough('let(a = 1, b = 2) a + b');
    });
  });
  describe('functions', function() {
    it('builtin function call', function() {
      exprPassthrough('abs(-4)');
    });
    it('function definition', function() {
      const e = 'function myFunc(arg1, arg2 = 42) = arg1 + arg2;\na = myFunc();';
      expect(parser.parse(e).toString()).to.equal(e);
    });
    it('function call with named args', function() {
      exprPassthrough('myFunc(arg2 = 2, arg1 = 7)');
    });
    it('function call with expression arg', function() {
      exprPassthrough('myFunc(5 - 3)');
    });
    it('function call with named expression arg', function() {
      exprPassthrough('myFunc(arg1 = 5 - 3)');
    });
  });
  describe('modules', function() {
    it('primitive', function() {
      passthrough('sphere();');
    });
    it('group', function() {
      passthrough('group();');
    });
    it('transform', function() {
      passthrough('translate();');
    });
    it('transform with one child', function() {
      passthrough('translate() sphere();');
    });
    it('transform with multiple children', function() {
      const e = 'translate() {\n'+
        '  sphere();\n'+
        '  cube();\n'+
        '}';
      passthrough(e);
    });
    it('for', function() {
      const e = 'for(i = [0 : 9]);';
      passthrough(e, 'for(i = [0 : 1 : 9]);');
    });
    it('for multiple assignments', function() {
      const e = 'for(x = [0 : 1], y = [2,3]);';
      passthrough(e, 'for(x = [0 : 1 : 1]) for(y = [2,3]);');
    });
    it('if', function() {
      passthrough('if(a) sphere(1);');
      passthrough('if(a) sphere(1); else sphere(2);');
      passthrough('group() { if(a) sphere(1); }',
                  'group() if(a) sphere(1);');
    });
    it('hull', function() {
      passthrough('hull() sphere();');
    });
    it('render', function() {
      passthrough('render() sphere();');
    });
    it('assignment in children', function() {
      const e = 'translate() {\n'+
        '  r = 5;\n'+
        '  sphere(5);\n'+
        '  cube();\n'+
        '}';
      passthrough(e);
    });
  });
  describe('user-defined modules', function() {
    it('basic', function() {
      passthrough('module MyModule() sphere();\nMyModule();');
    });
    it('param', function() {
      passthrough(
`module MyModule(r = 5) {
  sphere(r);
  cube(r);
}
MyModule(10);`);
    });
    it('assignment in children', function() {
      passthrough(
`module MyModule() {
  r = 5;
  sphere(5);
  cube();
}`);
    });
    it('inner modules', function() {
      passthrough(
`module MyModule() {
  module InnerModule() sphere(5);
  InnerModule();
}`);
    });
    it('no module body', function() {
      passthrough('module MyModule();');
    });
    it('empty module body', function() {
      const e = 'module MyModule() { }';
      passthrough(e, 'module MyModule();');
    });
  });
  describe('misc', function() {
    it('// comment', function() {
      const e = '// Some comment\na = 5;';
      expect(parser.parse(e).toString()).to.equal('a = 5;');
    });
    it('/* comment */', function() {
      const e = '/* Some\ncomment*/\na = 5;';
      expect(parser.parse(e).toString()).to.equal('a = 5;');
    });
    it('include', function() {
      passthrough('include <somefile.scad>');
    });
    it('use', function() {
      passthrough('use <somefile.scad>');
    });
    it('stray semicolon', function() {
      const e = 'sphere();;';
      passthrough(e, 'sphere();');
    });
    it('# modifier', function() {
      passthrough('#sphere();');
      passthrough('#if(1) sphere();');
    });
    it('% modifier', function() {
      passthrough('%sphere();');
    });
    it('% and # modifier', function() {
      passthrough('%#sphere();');
    });
    it('! modifier', function() {
      passthrough('!sphere();');
    });
    it('* modifier', function() {
      passthrough('*sphere();');
    });
  });
});
