// dynamicparser exposes the global OpenSCAD.parser - only used for testing
export const parser = global.OpenSCAD && global.OpenSCAD.parser || require('../lib/parser').default;

export const resourceLoader = global.OpenSCAD && global.OpenSCAD.resourceLoader;
