import fs from 'fs';
import fsp from 'fs-promise';
import gather from 'gather-stream';
import {parser, resourceLoader} from './dynamicparser';

export function readAll(file) {
  if (file) {
    if (!fs.existsSync(file) || !fs.statSync(file).isFile()) {
      return Promise.reject(`File not found: ${file}`);
    }
    return fsp.readFile(file, 'utf8');
  }

  return new Promise((resolve, reject) => {
    process.stdin.pipe(gather((err, buffer) => {
      if (err) return reject(err);
      return resolve(buffer.toString('utf8'));
    }));
  });
}

export function writeAll(file, contents) {
  if (arguments.length === 1) {
    contents = file;
    file = null;
  }
  if (file) return fs.writeFileSync(file, contents);
  console.log(contents.toString());
}
