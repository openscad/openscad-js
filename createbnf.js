var fs = require("fs");
var path = require('path');
var jison = require("jison");
var _ = require('lodash');

var opts = require("nomnom")
  .script('createbnf')
  .option('file', {
    flag: true,
    position: 0,
    help: 'file containing a grammar'
  })
  .option('outfile', {
    abbr: 'o',
    help: 'Filename of the generated BNF grammar'
  })
  .parse();

function prettyprint(parser, stream) {
  parser.productions.forEach(function(production) {
    var symbols = _.map(production.handle, function(symbol) {
      if (parser.terminals_.hasOwnProperty(parser.symbols_[symbol])) {
        if (symbol.length == 1) return "'" + symbol + "'";
        else return symbol;
      }
      else return symbol;
    });
    if (production.symbol !== '$accept') {
      stream.write(production.symbol + ' ::= ' + symbols.join(' ') + '\n');
    }
  });
}

main = function() {
  if (opts.file) {
    var bnf = fs.readFileSync(path.normalize(opts.file), "utf8");
    var parser = new jison.Parser(bnf);
    var stream;
    if (opts.outfile) stream = fs.createWriteStream(opts.outfile);
    else stream = process.stdout;
    prettyprint(parser, stream);
  }
}

if (require.main === module) main();
