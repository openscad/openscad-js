import * as AST from './ast';
import {ParserError} from './error';

let parser;

try {
  parser = require('./openscad.js').parser;
  parser.yy = {
    AST: AST,
    lexer: parser.lexer,
    parseError: (str, hash) => {
      throw new ParserError({type: 'error', text: str, sourceidx: [[hash.loc.first_line, hash.loc.first_column], [hash.loc.last_line, hash.loc.last_column]]});
    }
  };
}
catch(e) {
  console.log('Warning: openscad.js not found');
}

export default parser;
